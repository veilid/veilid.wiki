Veilid (pronounced Vay-Lid, from 'Valid and Veiled Identification') is an open-source, peer-to-peer, mobile-ﬁrst, networked application framework. 

Veilid allows anyone to build a distributed, private app. Veilid gives users the privacy to opt out of data collection and online tracking. Veilid is being built with user experience, privacy, and safety as our top priorities. It is open source and available to everyone to use and build upon. 

We believe that everyone should be able to forge relationships, learn, create, and build online — without being monetized. 

**[Contributor and Community Code of Conduct](https://veilid.com/contribute/code-of-conduct/)**

- [Gitlab-Flavored Markdown Guide](https://docs.gitlab.com/ee/user/markdown.html)
- [Wiki Specific Markdown](https://docs.gitlab.com/ee/user/markdown.html#wiki-specific-markdown)

# Getting Started

Please recursively check out the project using this command

`git clone git@gitlab.com:veilid/veilid.git`

Then read the [development](https://gitlab.com/veilid/veilid/-/blob/main/DEVELOPMENT.md) guide to get started. 
